# DEPLOYMENT OF A CSGO 2 SERVER

we are deploying a csgo server by using a docker-compose that uses the joedwards32/cs2 image

![CSGO](docs/cs2.jpg)

## REQUIREMENTS

- Docker + compose plugin

## CONFIG SERVER

You can change options in the docker-compose.yml:

```yml
    environment:
    # Server configuration
      - STEAMAPPVALIDATE=0          # (0 - no validation, 1 - enable validation)
      - CS2_SERVERNAME=garagenum-srv:CSGO     # (Set the visible name for your private server)
      - CS2_CHEATS=0                # (0 - disable cheats, 1 - enable cheats)
      - CS2_PORT=27015              # (CS2 server listen port tcp_udp)
      - CS2_SERVER_HIBERNATE=0      # (Put server in a low CPU state when there are no players. 0 - hibernation disabled, 1 - hibernation enabled)
      - CS2_RCON_PORT               # (Optional, use a simple TCP proxy to have RCON listen on an alternative port. Useful for services like AWS Fargate which do not support mixed protocol ports.)
      - CS2_IP=""                   #(CS2 server listening IP address, 0.0.0.0 - all IP addresses on the local machine, empty - IP identified automatically)
      - CS2_LAN=1                   # (0 - LAN mode disabled = WAN, 1 - LAN Mode enabled = LAN only)
      #- CS2_RCONPW=csgo92GAnum!         # (RCON password)
      - CS2_PW=changeme             # (CS2 server password)
      - CS2_MAXPLAYERS=20      # (Max players)
      - CS2_ADDITIONAL_ARGS         # (Optional additional arguments to pass into cs2)
      - CS2_CFG_URL                 # HTTP/HTTPS URL to fetch a Tar Gzip bundle of configuration files/mods
      # Game modes
      - CS2_GAMEALIAS               # (Game type, e.g. casual, competitive, deathmatch. See https://developer.valvesoftware.com/wiki/Counter-Strike_2/Dedicated_Servers)
      - CS2_GAMETYPE=0              # (Used if CS2_GAMEALIAS not defined. See https://developer.valvesoftware.com/wiki/Counter-Strike_2/Dedicated_Servers)
      - CS2_GAMEMODE=1              # (Used if CS2_GAMEALIAS not defined. See https://developer.valvesoftware.com/wiki/Counter-Strike_2/Dedicated_Servers)
      - CS2_MAPGROUP=mg_active      # (Map pool)
      - CS2_STARTMAP=de_inferno     # (Start map)
      # Bots
      - CS2_BOT_DIFFICULTY="1"          # (0 - easy, 1 - normal, 2 - hard, 3 - expert)
      - CS2_BOT_QUOTA="4"               # (Number of bots)
      - CS2_BOT_QUOTA_MODE="competitive"          # (fill, competitive)
      # TV
      - TV_AUTORECORD=0             # Automatically records all games as CSTV demos: 0=off, 1=on.
      - TV_ENABLE=0                 # Activates CSTV on server: 0=off, 1=on.
      - TV_PORT=27020               # Host SourceTV port
      #- TV_PW=tvGA92!              # CSTV password for clients
      #- TV_RELAY_PW=tvGA92!        # CSTV password for relay proxies
      - TV_MAXRATE=0                # World snapshots to broadcast per second. Affects camera tickrate.
      - TV_DELAY=0                  # CSTV broadcast delay in seconds
      # Logs
      - CS2_LOG=on                  # 'on'/'off'
      - CS2_LOG_MONEY=0             # Turns money logging on/off: (0=off, 1=on)
      - CS2_LOG_DETAIL=0            # Combat damage logging: (0=disabled, 1=enemy, 2=friendly, 3=all)
      - CS2_LOG_ITEMS=0             # Turns item logging on/off: (0=off, 1=on)
```

> We are not using a token because for now we are hosting the server on a LAN 
and we don't need the VAC (this is a protection against cheaters deployed by VALVE )
But if you want to use the token you can add this line at the top of the docker-compose file 
Note that you need to have a clean and not locked account on steam.

```
SRCDS_TOKEN=""              (Game Server Token from https://steamcommunity.com/dev/managegameservers)
```

### PLAY LOCALLY

```yml
      - CS2_LAN=1               # LAN ONLY
      - CS2_RCONPW=changeme     # ADMIN password
      - CS2_PW=changeme         # CS2 server password (optionnal)
```

### PLAY OVER INTERNET

1. static ip

Give a static IP to your server via your internet box (router)

2. ports redirection

You need to redirect the ports 27015 TCP and UDP and 27020 TCP from your internet box to your server

3. change configuration
```yml
      - CS2_LAN=0              # WAN (server accessible from outside the LAN)
      - CS2_RCONPW=changme     # ADMIN password
      - CS2_PW=changeme        # CS2 SERVER password (mandatory!!!)
```

4. You can connect to the server over internet using the console command with the public IP of your internet box and the password configured (see below)

## START THE SERVER 

```bash
docker compose up -d
```

**Check the logs when starting the container, it should download the game. If there are permission problems, make this command:**

```bash
chown -R $(whoami):$(whoami) cs2/
```

## CONNECT CLIENT TO SERVER

To connect to the server, you need to activate the console in the game settings

![conectiondirect](docs/csgo2-1.png)

Then you can open the console by pressing the 2 key (the little one just below the escape key at the left of the 1 key), and type in:

```
connect csgo2.legaragenumerique.xyz; password changeme
```

![conectiondirect](docs/csgo2-2.png)
